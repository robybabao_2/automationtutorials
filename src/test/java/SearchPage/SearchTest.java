package SearchPage;

import PageObject.SearchObject;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import util.Search;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class SearchTest extends DriverFactory {
    @BeforeTest
    public void loadPage() throws IOException {
        getDriver().get(Search.URL);
        getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void searchText() throws IOException, InterruptedException {
        new SearchObject(getDriver()).enterSearch(Search.TEST);
        new SearchObject(getDriver()).checkResult();
        getDriver().quit();
    }
}
