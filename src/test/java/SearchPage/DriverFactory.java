package SearchPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class DriverFactory {
    private static WebDriver driver;

    public static String driverPath() throws IOException {
        String driverConfigPath = System.getProperty("user.dir").concat("\\src\\driver.properties");
        Properties driverProperties = new Properties();
        driverProperties.load(new FileInputStream(driverConfigPath));

        return driverProperties.getProperty("driverPath");
    }

    public static WebDriver getDriver() throws IOException {
        String browser = new PropertyReader().readProperty("browser");
        if(driver == null) {
            if(browser.equals("firefox")){
                driver = createFirefoxDriver();
                driver.manage().window().maximize();
            }else if (browser.equals("chrome")){
                driver = createChromeDriver();
                driver.manage().window().maximize();
            }
        }
        return driver;
    }

    private static WebDriver createFirefoxDriver() throws IOException {
        System.setProperty("webdriver.gecko.driver", driverPath());
        return new FirefoxDriver();
    }

    private static WebDriver createChromeDriver() throws IOException {
        System.setProperty("webdriver.chrome.driver", driverPath());
        return new ChromeDriver();
    }

    public void destroyDriver() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.quit();
        driver = null;
    }
}
