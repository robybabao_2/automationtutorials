package PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import util.Search;

public class SearchObject {
    WebDriver driver;

    @FindBy(xpath = Search.SEARCH_FIELD)
    WebElement searchField;

    @FindBy(xpath = Search.SEARCH_SUGGESTION)
    WebElement searchSuggestion;

    @FindBy(xpath = Search.SEARCH_RESULT)
    WebElement searchResult;

    public SearchObject(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void enterSearch(String text) throws InterruptedException {
        searchField.click();
        searchField.sendKeys(text);
        Thread.sleep(20000);
        searchSuggestion.click();
    }

    public void checkResult(){
        Assert.assertTrue(searchResult.isDisplayed());
    }

}
