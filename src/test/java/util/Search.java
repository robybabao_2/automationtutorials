package util;

public class Search {
    //URL
    public static final String URL = "https://www.google.com";

    //user input
    public static final String TEST = "test";

    //list of elements
    public static final String SEARCH_FIELD = "//input[@name='q']";
    public static final String SEARCH_SUGGESTION = "//*[@id=\"tsf\"]/div[2]/div/div[2]/div[2]/ul/li[1]/div[1]/div/span";
    public static final String SEARCH_RESULT = "//*[@id=\"rso\"]/div/div/div[1]/div/div/div[1]/a/h3";
}
